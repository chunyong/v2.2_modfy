/*

	SDS API - Comman Functions
	Do not make Changes Without Notice in this API, it may AFFECT System Structure/Flow
	==============================================================================================================
	| Code		| Username		| Date			| Function Name		| Remarks
	| FUN01		| ChunYong 		| 08/06/2017	| -			| Add for connection doctor

	===============================================================================================================

*/
	/** PreLoad Language Transalate **/

	var commandlist = {
			"checkSQL" : "Check connection with Database",
			"/help"		: "return a list of supported command"
	};
	
	self.onmessage = function(msg){
		if (commandlist.hasOwnProperty(msg.data) || msg.data == 'bypass'){
			switch (msg.data) {
				case "/help":
					postMessage(commandlist);
					break;
				case "checkSQL":
					var xhttp = new XMLHttpRequest();
					xhttp.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							postMessage(this.responseText);
						}
					};
					xhttp.open("GET", "../php/Function/ConChkCaller.php", true);
					xhttp.send();
					break;
				case "bypass":
					postMessage(msg.data);
					break;
				default:

			}
		}else{
			postMessage("Unsupported command. Type /help for more information.");
		}
	}
