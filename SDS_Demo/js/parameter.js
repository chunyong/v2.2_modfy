/*

	SDS API - Comman Functions
	Do not make Changes Without Notice in this API, it may AFFECT System Structure/Flow
	==============================================================================================================
	| Code		| Username		| Date			| Function Name		| Remarks

	===============================================================================================================

*/
	/** PreLoad Language Transalate **/

	/* SDS Console Script Path*/
	var script_path = {
		"Transaction Search" : "PHP/SQL/LOG/TR_Transaction.php",
		"HTTP Search"				 : "PHP/SQL/LOG/XS_Transaction.php",
		"Request Detail"		 : "PHP/SQL/LOG/RQ_Transaction.php",
		"Response Detail"		 : "PHP/SQL/LOG/RS_Transaction.php",
		"Transaction Detail" : "PHP/SQL/LOG/TR_DTL_Transaction.php",
		"Transaction Performance" : "PHP/SQL/LOG/TR_PFM_Transaction.php",
		"Com Data"					 : "PHP/SQL/LOG/EXTRA_Transaction.php",
		"Com Detail"					 : "PHP/SQL/LOG/RAW_Transaction.php",
		"Composite Search"		: "php/SQL/LOG/TR_CTransaction.php",
		"Composite Detail"		: "php/SQL/LOG/TR_COMP_Transaction.php",
		"Timeout Search"			: "php/SQL/LOG/TMO_TR_Transaction.php",
		"SMS Report"					: "PHP/SQL/LOG/SMS_Transaction.php",
		"Watchlist"						: "PHP/SQL/LOG/Watchlist_Transaction.php",
		"Add Watchlist"				: "PHP/SQL/LOG/Watchlist_Transaction_Add.php",
		"Remove Watchlist"		: "PHP/SQL/LOG/Watchlist_Transaction_DLT.php",
		"Watchlist Detail"		: "PHP/SQL/LOG/Watchlist_Transaction_Detail.php",
		"Error Search"					: "PHP/SQL/LOG/ER_Transaction.php",
		"Event Count"					: "PHP/SQL/LOG/CountEV_Transaction.php"
	};
