/*

	SDS API - Comman Functions
	Do not make Changes Without Notice in this API, it may AFFECT System Structure/Flow
	==============================================================================================================
	| Code		| Username		| Date			| Function Name		| Remarks
	| FUN01		| KhaiMeng		| 07/01/2016	| genXML			| Generate XML for MessageBody
	| FUN02		| KhaiMeng		| 07/01/2016	| ajaxSubmit		| Ajax Fire to PHP
	| FUN03		| KhaiMeng		| 28/01/2016	| langTranslator	| Language Transalator
	| FUN04		| KhaiMeng		| 23/02/2016	| setMenuPath		| Set Menu Path/Hyperlink
	| FUN05		| ChunYong		| 04/05/2016	| Hex2Str			| Hex to string (ASCII)
	| FUN06		| ChunYong		| 12/05/2016	| Format Date		| Format date
	| FUN07		| ChunYong		| 12/05/2016	| Format Time		| Format time
	| FUN08		| ChunYong		| 12/05/2016	| ClearSession		| Clear Session data
	| FUN09		| ChunYong		| 24/05/2016	| getRequireEmptyField	| Get empty required field ID in array
	| FUN10		| ChunYong		| 26/08/2016	| textarea tab		| Add tab function to tab
	| FUN11		| ChunYong		| 05/09/2016	| SDS Field Type	| Covert SDS Field type
	| FUN12		| ChunYong		| 05/09/2016	| getSDSFieldType	| Get SDS Field type name
	| FUN13		| ChunYong		| 21/09/2016	| downloadInnerHtml | Download data as text
	| FUN14		| ChunYong		| 05/10/2016	| copyText			| Copy text in the element
	| FUN15		| ChunYong		| 07/02/2017	| setBackPath		| For Back button used without updating user activity
	| FUN16		| ChunYong		| 07/02/2017	| getColorPlater | For Analytics used generate random color
	| FUN17		| ChunYong		| 26/04/2017	| logout					| For logout use
	| FUN18		| ChunYong		| 26/04/2017	| userValidation	| Added for validate user and Generate Menu
	| FUN19		| ChunYong		| 31/05/2017	| startUp					| Use for autologin on every system
	| Fun20		| ChunYong		| 31/05/2017	| userLogin				| Use for user login on every system
	| Fun21 	| ChunYong		| 24/04/2018	| showMainLoader	| Use to show main loader
	| Fun22		| ChunYong		| 24/04/2018	| hideMainLoader	| Use to hide main loader
	| Fun23		| ChunYong		| 24/04/2018	| initLoader			| Use to set small loader
	===============================================================================================================

*/
	/** PreLoad Language Transalate **/

	//FUN01 <start>
	/** Generate XML (MessageBody) **/
	function genXML(option){
		var opt  = {
			formID :'',
		};
		$.extend(opt, option);

		var x = document.getElementById(opt.formID);
		var i;
		var dataString = 'xml=';

		for (i = 0; i < x.length; i++)
		{
			dataString = dataString + '<' + x.elements[i].name + '>' + x.elements[i].value + '</' + x.elements[i].name + '>';
		}
	return dataString;
	};
	//FUN01 <end>


	//FUN02 <strat>
	/** Ajax Fire **/
	function ajaxSubmit(option){
		var opt  = {
			formID :'',
			tableID :'',
			transCode :'',
			path :'',
			action :'',
			callBack :'',
			userID :'',
			enc :'',
			url :'',
			pageID :'',
		};
		$.extend(opt, option);

		if (opt.formID != null){
			var dataString = genXML({formID: opt.formID});
		}

		$('#' + opt.formID).submit(function(e)
		{
			$.ajax(
			{
				url : opt.url,
				type: 'POST',
				data : {tc:opt.transCode, path:opt.path, xml:dataString, userID:opt.useID, enc:opt.enc, pageID:opt.pageID},
				success:function(dataJson)
				{
					if(opt.action == 'preload')
					{
						opt.callBack(dataJson);
						//$.notify('I am a warning box.', 'danger');
					} else {
						$('#'+ opt.tableID).setGridParam({datastr: dataJson, datatype:'jsonString', page:1}).trigger('reloadGrid');
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//error handling
				}

			});
			e.preventDefault();
			$(this).unbind(e);
			});
		$('#' + opt.formID).submit();
	};
	//FUN02 <end>

	//FUN03 <Start>
	function langTranslator(option){
		var opt  = {
			lang :'',
			field :'',
		};
		$.extend(opt, option);

		var x = $(opt.field);
		var i;

		for (i = 0; i < x.length; i++)
		{
			var defLang = x.eq(i).html();
			var iValue = i;

			langAjax({
				lang : opt.lang ,
				field : opt.field ,
				def : defLang ,
				iValue : iValue ,
			});
		}
	}

	function langAjax(option){
		var opt  = {
			lang : '' ,
			field : '' ,
			def : '' ,
			iValue : '' ,
		};
		$.extend(opt, option);

		$.ajax(
		{
			url : 'Translator/lang.php',
			type: 'POST',
			data : {lang:opt.lang, def:opt.def},
			success:function(dataJson)
			{
				if (dataJson != ''){
					$(opt.field).eq(opt.iValue).html(dataJson);
				}
			},
		});
	}
	//FUN03 <end>


	//FUN04 <start>
	function setMenuPath(option){
		var opt  = {
			id : '' ,
			path : '' ,
			menu : ''
		};
		$.extend(opt, option);

		$('#'+opt.id).load(opt.path);
		//For Application UI used appended title at top like (SDS Analytics)
		$("#nav_page_title").text(opt.menu);

		sessionStorage.clear();
	}
	//FUN04 <end>

	//FUN05 <start>
	// Initial calander function
	function Hex2Str(hex) {
    	var str = '';
   	 	for (var i = 0; i < hex.length; i += 2) {
        	var v = String.fromCharCode(parseInt(hex.substr(i, 2),16));
        	str += v;
   	 	}
   	 	return str;
	}
	//FUN05 <end>

	//FUN06 <start>
	//format date function
	function formatDate(date){
		var tempDate = "";
		tempDate = date.substr(i,4);
		for (var i = 4; i < date.length; i+= 2)
		{
			tempDate = tempDate + "-" + date.substr(i,2);
		}
		return tempDate;
	}
	//FUN06 <end>

	//FUN07 <start>
	//Format time
	function formatTime(time){
  	var tempTime = "";
  	var pad = "000000";
  	if (time.length < 6)
  	{
  		time = "0" + time;
  		time = pad.substring(0, pad.length - time.length) + time
  	}
    for (var i = 0 ; i < time.length ; i+= 2){
			tempTime = tempTime + time.substr(i,2) + ":";
		}

		if (tempTime.trim().substr(tempTime.length-1,1) == ":"){
			tempTime = tempTime.substr(0,tempTime.length-1);
		}
    return tempTime;
    }
	//FUN07 <end>

	//FUN08 <start>
	//clear session storage
    function clearSession(){
    	sessionStorage.clear();
    }
	//FUN08 <end>

	//FUN09 <start>
	function getRequireEmptyField(form)
	{
		var R_ID = [];
    	$("#"+form+" r").find("input").each(function()
    		{
    			if ($(this).val() == '')
    			{
    				R_ID.push(this.id);
    			}
    	});

    	$("#"+form+" r").find("select").each(function()
    		{
    			if ($(this).val() == '')
    			{
    				R_ID.push(this.id);
    			}
    	});

		return R_ID;
	}
	//FUN09

	//FUN10 <start>
	$(document).delegate('textarea', 'keydown', function(e) {
	  var keyCode = e.keyCode || e.which;

	  if (keyCode == 9) {
		e.preventDefault();
		var start = $(this).get(0).selectionStart;
		var end = $(this).get(0).selectionEnd;

		// set textarea value to: text before caret + tab + text after caret
		$(this).val($(this).val().substring(0, start)
					+ "\t"
					+ $(this).val().substring(end));

		// put caret at right position again
		$(this).get(0).selectionStart =
		$(this).get(0).selectionEnd = start + 1;
	  }
	});
	//FUN10 <end>

	//FUN11 <start>
	function SDSFieldTyp(val){
		switch(val){
			case '0' :
				return 'Binary';
			case '1' :
				return 'Bitmap';
			case '2' :
				return 'Bitmap (packed)';
			case '11' :
				return 'Numeric';
			case '12' :
				return 'Numeric ISO (packed)';
			case '13' :
				return 'Numeric IBM';
			case '14' :
				return 'Numeric IBM (packed)';
			case '15' :
				return 'Numeric (short)';
			case '16' :
				return 'Numeric (Integer)';
			case '17' :
				return 'Numeric (Long)';
			case '21' :
				return 'Character';
			case '22' :
				return 'Character (Variable Length)';
			case '23' :
				return 'Boolean';
			default :
				return val;
		}
	}
	//FUN11 <end>

	//FUN12 <start>
	function getSDSFieldType(val){
		switch(val){
			case 'Binary' :
				return '0';
			case 'Bitmap' :
				return '1';
			case 'Bitmap (packed)' :
				return '2';
			case 'Numeric' :
				return '11';
			case 'Numeric ISO (packed)' :
				return '12';
			case 'Numeric IBM' :
				return '13';
			case 'Numeric IBM (packed)' :
				return '14';
			case 'Numeric (short)' :
				return '15';
			case 'Numeric (Integer)' :
				return '16';
			case 'Numeric (Long)' :
				return '17';
			case 'Character' :
				return '21';
			case 'Character (Variable Length)' :
				return '22';
			case 'Boolean' :
				return '23';
			default :
				return val;
		}
	}
	//FUN12 <end>

	//FUN13 <start>
	function downloadInnerHtml(filename, field) {
		var elHtml = $("#" + field).text();
		var link = document.createElement('a');
		mimeType = 'text/plain';

		link.setAttribute('download', filename);
		link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(elHtml));
		link.click();
	}
	//FUN13 <end>

	//FUN14 <start>
	function copyText(field){
		var range = "";

		if (document.selection) {
			document.selection.empty();
			range = document.body.createTextRange();
			range.moveToElementText($("#" + field)[0]);
			range.select().createTextRange();
			document.execCommand("Copy");

		} else if (window.getSelection) {
			window.getSelection().removeAllRanges();

			range = document.createRange();
			range.selectNode($("#" + field)[0]);
			window.getSelection().addRange(range);
			document.execCommand("Copy");
		}
	}
	//FUN14 <end>

	//FUN15 <start>
	function setBackPath(option){
		var opt  = {
			id : '' ,
			path : '' ,
			menu : ''
		};
		$.extend(opt, option);

		$('#'+opt.id).load(opt.path);
	}
	//FUN15 <end>

	//FUN16 <start>
	function getColorPlater(size){
		var collist = [	'213,197,200', '141,153,174', '141,137,166', '121,180,169', '149,184,209',
						  	'30,21,42', '78,103,102', '90,177,187', '165,200,130', '247,221,114',
						  	'191,237,239', '84,92,82', '153,197,181', '93,115,126', '220,196,142',
						  	'212,224,155', '170,174,127', '88,75,83', '242,233,228', '221,240,255',
						  	'188,255,219', '238,227,171', '174,236,239', '29,120,116', '99,176,205'];
		var rtlist = [];
		for (var i = collist.length; i; i--) {
			var j = Math.floor(Math.random() * i);
			var x = collist[i - 1];
			collist[i - 1] = collist[j];
			collist[j] = x;
		}

		for (var t = 0; t < size; t++){
			rtlist.push(collist[t]);
		}
		return rtlist;
	}
	//FUN16 <end>

	//FUN17 <start>
	function logout(){
		$.ajax({
					url : 'PHP/SQL/UTIL/DestroySession.php',
					type: "POST",
					data : "",
					success:function(data)
					{
						$.get('index.html', function(data){
						document.open();
						document.write(data);
						document.close();
						$.cache = {};
					}, "text");
					localStorage.clear();
					}
		});
	}
	//FUN17 <end>

	//FUN18 <start>
	function userValidation(){
		$.ajax({
				url : 'PHP/SQL/UTIL/GetSession.php',
				type: "POST",
				data : "",
				success:function(data)
				{
					try{
						data = JSON.parse(data);
						$("#U_Name").text(data.NAME);
						$("#L_ID").text(data.LOGIN_ID);
					}
					catch(err){
						alert(err);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#simple-msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
				}
			});

			$.ajax({
					url : 'PHP/SQL/UTIL/Menu.php',
					type: "POST",
					success:function(data){
						document.getElementById('side-menu').innerHTML += data;
						var s = document.createElement('script');
						s.type = 'text/javascript';
						s.async = true;
						s.src = 'js/inspinia.js';
						var header_scripts = document.getElementsByTagName('script')[0];
						header_scripts.parentNode.insertBefore(s, header_scripts);
						var s1 = document.createElement('script');
						s1.type = 'text/javascript';
						s1.async = true;
						s1.src = 'js/plugins/pace/pace.min.js';
						var header_scripts1 = document.getElementsByTagName('script')[0];
						header_scripts1.parentNode.insertBefore(s1, header_scripts);
						$("#mainContent").load("pages/SDS_HOME/About_Us.html");
						hideMainLoader(1000);
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
						$("#simple-msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
					}
			});
	}

	//FUN18 <end>

	//FUN19 <start>
	function startUp(sys){
		$.ajax({
			"url"	: "PHP/SQL/UTIL/CheckUser.php",
			"type"	: "POST",
			data : {
					"sys"		: sys
				},
			success: function(data){
				try{
					data = JSON.parse(data);
					if (data.Status == 'AA'){
						hideMainLoader(500);
						$.get('pages/SDS_Home/Main.html', function(data){
							document.open();
							document.write(data);
							document.close();
							$.cache = {};
						}, "text");
					}
					else if (data.Status == 'AB'){
						$ermsg = data.Data;
						Command: toastr["error"]($ermsg);
					}
					else{
						localStorage.clear();
					}
				}catch(err){
					alert(err);
				}
				finally{
					hideMainLoader(0);
				}
			}
		});
	}
	//FUN19 <end>

	//FUN20 <start>
	function userLogin(username, password, sys){
		$.ajax({
			"url"	: "PHP/SQL/UTIL/ValidateUser.php",
			"type"	: "POST",
			"data"  : {
							"username"	: username,
							"password"	: password,
							"sys"		: sys
			},
			success: function(data){
				localStorage.clear();
				try{
					data = JSON.parse(data);
					if (data.Status == 'AA'){
						localStorage.login = 'Y';
						$.get('pages/SDS_Home/Main.html', function(data){
							document.open();
							document.write(data);
							document.close();
							$.cache = {};
						}, "text");
					}
					else{
						Command: toastr["error"](data.Data);
					}
				}catch(err){
					alert(data);
				}
				finally{
					hideMainLoader(0);
				}
			}
		});
	}
	//FUN20 <end>

	//FUN21 <start>
	function showMainLoader(){
		$("#loading_box, #loading_overlay").css("display","block");
		$("#loading_overlay").css("opacity","1.1");
	}
	//FUN21 <end>

	//FUN22 <start>
	function hideMainLoader(delay){
		setTimeout(function(){
			$("#loading_box, #loading_overlay").css("display","none");
			$("#loading_overlay").css("opacity","-0.13");
		}, delay);
	}
	//FUN22 <end>

	//FUN23 <start>
	function initLoader(loaderid){
		return new SVGLoader( document.getElementById( loaderid ), { speedIn : 500, easingIn : mina.easeinout } );
	}
	//FUN23 <end>
