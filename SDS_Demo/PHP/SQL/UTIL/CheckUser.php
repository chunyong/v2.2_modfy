<?php

//Initial Value
$stmt = null;
$result = array();
session_start();

$_POST['ACT'] = 'INQ';
$_POST['DBTYP'] = 'SYS';
$username = '';
if(isset($_SESSION['LOGIN_ID'])):
	$username = $_SESSION['LOGIN_ID'];
else:
	$result['Status'] = 'AC';
	$result['Data'] = "dummy Error: ";
	die(json_encode($result,JSON_UNESCAPED_SLASHES));
endif;

$stmt = "Select U.LOGIN_ID, U.NAME, U.PASSWORD, G.NAME as UGROUP, G.GROUP_ID from USERS U inner join USER_GROUP UG on U.USER_ID = UG.USER_ID inner join U_GROUP G on UG.GROUP_ID = G.GROUP_ID";
$stmt .= " Where U.LOGIN_ID = '$username' and U.ACTIVE = 'Y' ";

$_POST['STMT'] = $stmt;
include '../../function/SQLCaller.php';
$rspresult = json_decode($_POST['result']);
$result = $rspresult->Data;
$pass = '';
$group = '';
$login = '';
$name = '';

if (sizeof($result) > 0){
	foreach($result as $key){
		$pass = $key->PASSWORD;
		$group = $key->UGROUP;
		$login = $key->LOGIN_ID;
		$name	= $key->NAME;
		$groupid = $key->GROUP_ID;
	}

	if ($group != 'Administrator'):
		$_POST['ACT'] = 'INQ';
		$_POST['DBTYP'] = 'SYS';
		$sys = $_POST['sys'];
		$sys = str_replace('SDS ', '', $sys);
		$stmt = " Select M.MENU_ID from MENU M inner join MENU_GROUP MG on M.MENU_ID = MG.MENU_ID inner join U_GROUP G on MG.GROUP_ID = G.GROUP_ID";
		$stmt .= " Where G.NAME = '$group' and G.GROUP_ID ='$groupid' and M.SYS = '$sys' ";
		$_POST['STMT'] = $stmt;
		include '../../function/SQLCaller.php';
		$rspresult = json_decode($_POST['result']);
		$result = $rspresult->Data;
		if (sizeof($result) > 0){
			$_SESSION['LOGIN_ID'] = $login;
			$_SESSION['GROUP'] = $group;
			$_SESSION['NAME'] = $name;
			$_SESSION['SYS'] = 'SDS ' . $_POST['sys'];
			$rspresult->Data = "Success Auto Login";
			echo json_encode($rspresult,JSON_UNESCAPED_SLASHES);
		}
		else{
			include '../../SQL/UTIL/DestroySession.php';
			$rspresult->Status = 'AB';
			$rspresult->Data = "Error : User has no authority in this system. Login not allowed.";
			die(json_encode($rspresult,JSON_UNESCAPED_SLASHES));
		}
	else:
		$_SESSION['LOGIN_ID'] = $login;
		$_SESSION['GROUP'] = $group;
		$_SESSION['NAME'] = $name;
		$_SESSION['SYS'] = 'SDS ' . $_POST['sys'];
		$rspresult->Data = "Success Auto Login";
		echo json_encode($rspresult,JSON_UNESCAPED_SLASHES);
	endif;
}
else{
	include '../../SQL/UTIL/DestroySession.php';
	$result->Status ='AB';
	$result->Data = "Error : Username not found or User is not active.";
	die(json_encode($result,JSON_UNESCAPED_SLASHES));
}

?>
