<?php

$sys = 'Config';

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';
$_POST['DBTYP'] = 'SYS';

$stmt = "Select M.MENU_ID, M.PARENT_ID, M.MENU_TITLE, M.MENU_ICON, M.HREF from MENU M ";
$stmt .= " Where M.SYS != '$sys' order by M.INDEX_LEVEL";

$_POST['STMT'] = $stmt;
include '../../function/SQLCaller.php';
$rspresult = json_decode($_POST['result']);

$rspArray =array();

$tempPId = null;
$tempSId = null;
$tempId = null;
$html = null;
$check = 0;
$second = 0;
$third= 0;

foreach($rspresult->Data as $mnu){
	$PId = $mnu->PARENT_ID;
	$Id = $mnu->MENU_ID;
	$Desc = $mnu->MENU_TITLE;
	$Murl = $mnu->HREF;
	$Mimg = $mnu->MENU_ICON;
	if($PId == 'root'):
		if ($check == 0){
			$html = $html . '<li class="sideMNU" Id="' . $Id . '">';
			$check = 1;
		}
		else{
			if($third == 1){
					$html = $html . '</ul>';
			}

			if($second == 1){
				$html = $html . '</ul>';
				$second = 0;
			}
			$html = $html . '</a>';
			$html = $html . '</li>';
			$html = $html . '<li class="sideMNU" Id="' . $Id . '">';
		}
		if ($Murl == ''){
			$html = $html . '<a href="#" title="' . $Desc . '" >';
		}
		else{
			$html = $html . '<a onclick="setMenuPath({id:\'mainContent\',path:\'' . $Murl . '\',menu:\'' . $Desc . '\'})" title="' . $Desc . '" >';
		}
		$html = $html . '<i class="fa ' .  $Mimg . '"></i> <span class="nav-label">' . $Desc . '</span>';
		$tempPId = $Id;
		$tempSId = null;
	elseif($PId == $tempPId && $tempSId == null):
		$html = $html . '<span class="fa arrow"></span>';
		$html = $html . '</a>';
		$html = $html . '<ul class="nav nav-second-level collapse">';
		if ($Murl == ''){
			$html = $html . '<li Id="' . $Id . '"><a href="" title="' . $Desc . '" ><i class="fa ' . $Mimg . '"></i>' . $Desc;
		}
		else{
			$html = $html . '<li Id="' . $Id . '"><a onclick="setMenuPath({id:\'mainContent\',path:\'' . $Murl . '\',menu:\'' . $Desc . '\'})" title="'.$Desc . '" ><i class="fa ' . $Mimg . '"></i>' . $Desc;
		}
		$tempSId = $Id;
		$second = 1;
	elseif($PId == $tempPId && $tempSId != null):
		if ($Murl == ''){
			$html = $html . '<li Id="' . $Id . '"><a href="" title="'.$Desc . '" ><i class="fa ' . $Mimg . '"></i>' . $Desc ;
		}
		else{
			$html = $html . '<li Id="' . $Id . '"><a onclick="setMenuPath({id:\'mainContent\',path:\'' . $Murl . '\',menu:\'' . $Desc . '\'})" title="'.$Desc . '" ><i class="fa ' . $Mimg . '"></i>' . $Desc ;
		}
		$tempSId = $Id;
	elseif($PId == $tempSId && $third == 0):
		$html = $html . '<span class="fa arrow"></span></a><ul class="nav nav-third-level collapse">';
		if ($Murl == ''){
			$html = $html . '<li Id="' . $Id . '"><a href="" title="' . $Desc . '" ><i class="fa ' . $Mimg . '"></i>' . $Desc . '</a></li>';
		}
		else{
			$html = $html . '<li Id="' . $Id . '"><a onclick="setMenuPath({id:\'mainContent\',path:\'' . $Murl . '\,menu:\'' . $Desc . '\',menu:\'' . $Desc . '\'})" title="' . $Desc . '" ><i class="fa ' . $Mimg . '"></i>' . $Desc . '</a></li>';
		}
		$third = 1;
	elseif($PId == $tempSId && $third == 1):
		if ($Murl == ''){
			$html = $html . '<li Id="' . $Id . '"><a href="" title="' . $Desc . '"><i class="fa ' . $Mimg . '"></i>' . $Desc . '</a></li>';
		}
		else{
			$html = $html . '<li Id="' . $Id . '"><a onclick="setMenuPath({id:\'mainContent\',path:\'' . $Murl . '\',menu:\'' . $Desc . '\'})" title="' . $Desc . '" ><i class="fa ' . $Mimg . '"></i>' . $Desc . '</a></li>';
		}
	endif;
	$tempId = $Id;
}
$html = $html . '</a></li></ul></li>';
echo $html;

?>
