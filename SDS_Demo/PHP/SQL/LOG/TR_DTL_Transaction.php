<?php

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';

$date =  $_POST['date_logd'];
$nextdate = date('Ymd',strtotime("$date +1 days"));
$refconn = $_POST['ref_conn'];
$refreq = $_POST['ref_reqs'];
$refnode = $_POST['ref_node'];
$servername = $_POST['server_name'] ;

$stmt = "Select kq.ph_type, kq.ph_mmap, kq.raw_data_in as kq_raw_data, ks.raw_data_ou as ks_raw_data from kq inner join ks on kq.ref_conn = ks.ref_conn and kq.ref_reqs = ks.ref_reqs and kq.ref_node = ks.ref_node and kq.server_name = ks.server_name ";
$stmt = $stmt . " Where kq.ref_conn = '$refconn ' and kq.ref_reqs = $refreq and kq.ref_node = $refnode and kq.server_name = '$servername' and kq.date_logd =$date and ks.date_logd between $date and $nextdate ";

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';

include '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);

$result = formatResult($result);
echo json_encode($result);


function formatResult($data){
  $result = $data->Data;
  foreach ($result as $key){
  	$domrq = new DOMDocument;
  	$domrs = new DOMDocument;
  	$domrq->preserveWhiteSpace = False;
  	$domrs->preserveWhiteSpace = False;
  	$domrq->formatOutput = True;
  	$domrs->formatOutput = True;
  	if (strpos(html_entity_decode((string)$key->kq_raw_data), '<') === 0){
  		$domrq->loadXML(html_entity_decode((string)$key->kq_raw_data));
  		$key->kq_raw_data = htmlentities(str_replace('<?xml version="1.0"?>' , '', $domrq->saveXml()));
  	}
  	else{
  		$key->kq_raw_data = $key->kq_raw_data;
  	}

  	if (strpos(html_entity_decode((string)$key->ks_raw_data), '<') === 0){
  		$domrs->loadXML(html_entity_decode((string)$key->ks_raw_data));
  		$key->ks_raw_data = htmlentities(str_replace('<?xml version="1.0"?>' , '', $domrs->saveXml()));
  	}
  	else{
  		$key->ks_raw_data = $key->ks_raw_data;
  	}
  }
  return $data;
}

?>
