<?php

//Initial Value
$stmt = null;
$searchContent = $_POST;
$contentAry = Array();

foreach ($searchContent as $key=>$value){
	if ($value != ''){
		$contentAry[$key] = $value;
	}
}

$stmt = formatSql($contentAry);

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';
$_POST['ACT'] = 'INQ';

include '../../function/SQLCaller.php';
$result = json_decode($_POST['result']);

echo json_encode($result);

function formatSql($contentAry){
	$stmt = "Select CONCAT( RTRIM(cast(ref_conn as char)) , ':' , RTRIM(cast(ref_reqs as char)) , ':' , RTRIM(cast(ref_node as char))) ref_conn, CONCAT( RTRIM(remote_ip) , ':' , RTRIM(cast(remote_port as char))) remote_ip , host_ref, date_reqs, time_reqs, method, respcd, url, server_name from xs Where date_reqs > 0";

	if (array_key_exists("ref_conn", $contentAry)){
		$ref = '';
		$ref_reqs = '';
		$ref_node = '';
		if (strpos($contentAry['ref_conn'], ':') !== false){
			$tempArry = array();
			$tempArry = explode(':',$contentAry['ref_conn']);
			$ref = $tempArry[0];
			$ref_reqs = $tempArry[1];
			$stmt .= " and ref_conn = '$ref' and ref_reqs = $ref_reqs ";
			if (sizeof($tempArry) > 2)
			{
				$ref_node = $tempArry[2];
				$stmt .= " and ref_node = $ref_node ";
			}
		}
		else{
			$ref = $contentAry['ref_conn'];
			$stmt .= " and ref_conn = '$ref' ";
		}
	}
	else{
		$date; $startdate; $enddate;
		//Check Date and form date in sql
		$date = date('Ymd');
		switch ($contentAry['srchdate']){
			case 'T' :
				$stmt .= " and date_reqs = $date ";
				break;
			case 'R' :
				if (array_key_exists("s_date_logd", $contentAry) && array_key_exists("e_date_logd", $contentAry)){
					$startdate = str_replace('-', '', $contentAry['s_date_logd']);
					$enddate = str_replace('-', '', $contentAry['e_date_logd']);
				}
				elseif (array_key_exists("s_date_logd", $contentAry) && !array_key_exists("e_date_logd", $contentAry)){
					$startdate = str_replace('-', '', $contentAry['s_date_logd']);
					$enddate = $date;
				}
				elseif (!array_key_exists("s_date_logd", $contentAry) && array_key_exists("e_date_logd", $contentAry)){
					$startdate = date('Ymd', strtotime('-1 year'));
					$enddate = str_replace('-', '', $contentAry['e_date_logd']);
				}
				else{
					$startdate = $date;
					$enddate = $date;
				}
				$stmt .= " and date_reqs between $startdate and $enddate ";
				break;
			case '7' :
				$startdate = date('Ymd', strtotime('-7 days'));
				$enddate = $date;
				$stmt .= " and date_reqs between $startdate and $enddate ";
				break;
			case '30' :
				$startdate = date('Ymd', strtotime('-30 days'));
				$enddate = $date;
				$stmt .= " and date_reqs between $startdate and $enddate ";
				break;
			default :
				$date = date('Ymd');
				$startdate = $date;
				$enddate = date('Ymd', strtotime('+1 days'));
				$stmt .= " and date_reqs = $startdate ";
				break;
			}

			if (array_key_exists("s_time_logd", $contentAry) && array_key_exists("e_time_logd", $contentAry)){
				$starttime = $contentAry["s_time_logd"];
				$endtime = $contentAry["e_time_logd"];
				$stmt .= " and time_reqs between $starttime and $endtime ";
			}

			if (array_key_exists("remote_ip", $contentAry)){
				$ip = '';
				$port = '';
				if (strpos($contentAry['remote_ip'], ':') !== false){
					$tempArry = explode(':',$contentAry['remote_ip']);
					$ip = $tempArry[0];
					$port = $tempArry[1];
					$stmt .= " and remote_ip = '$ip' and remote_port = $port ";
				}
				else{
					$ip = $contentAry['remote_ip'];
					$stmt .= " and remote_ip = '$ip' ";
				}
			}

			if (array_key_exists("method", $contentAry)){
				$stmt .= " and method  = '" . $contentAry['method'] . "' ";
			}
			$stmt .= ' order by date_reqs desc, time_reqs desc, ref_conn desc , ref_reqs desc ';
	}
	return $stmt;
}

?>
