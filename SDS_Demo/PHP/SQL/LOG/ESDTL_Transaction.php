<?php

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';

$stmt = "Select kq.date_logd, kq.time_logd, ks.host_ref, ( RTRIM(cast(kq.ref_conn as char)) + ':' + RTRIM(cast(kq.ref_reqs as char))) ref_conn, kq.host_sid, kq.host_did, kq.ph_webservice, kq.host_txcd, ks.host_sts, kq.trx_cif, ks.comp_flag, kq.trx_act, kq.trx_amt1 from kq inner join ks on  kq.ref_conn = ks.ref_conn and kq.ref_reqs = ks.ref_reqs and kq.ref_node = ks.ref_node where kq.entry_type = 'S' and ks.entry_type = 'R' and ks.comp_flag in ('C', 'A', 'R', 'T')";

$stmt = $stmt . " and kq.ref_conn = '" . $_POST['RRref'] . "' and kq.ref_reqs = '" . $_POST['RReq'] . "'";

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';

include '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);

foreach($result as $key)
{
	$temp = $key->comp_flag;
	if($temp === 'A')
	{
		$key->comp_flag = 'Accepted';
	}
	elseif ($temp === 'R')
	{
		$key->comp_flag = 'Rejected';
	}
	elseif ($temp === 'C')
	{
		$key->comp_flag = 'Completed';
	}
	elseif ($temp === 'T')
	{
		$key->comp_flag = 'Timeout';
	}
}

echo json_encode($result);

?>