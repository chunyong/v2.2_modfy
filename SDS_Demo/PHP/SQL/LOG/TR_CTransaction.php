<?php

//Initial Value
$stmt = null;
$result = array();
$searchContent = $_POST;
$contentAry = Array();

foreach ($searchContent as $key=>$value){
	if ($value != ''){
		$contentAry[$key] = $value;
	}
}

$stmt = formatSql($contentAry);

$_POST['ACT'] = 'INQ';
$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';

include '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);

$result = formatResult($result);

echo json_encode($result);


function formatSql($contentAry){
	$stmt = "Select kq.date_logd, kq.time_logd, kq.ref_node, kq.host_sid, kq.host_did, kq.ph_webservice, kq.ph_webservice_type, ks.host_sts, kq.trx_cif, ks.host_ref, CONCAT( RTRIM(cast(kq.ref_conn as char)) , ':' , RTRIM(cast(kq.ref_reqs as char)) , ':' , RTRIM(cast(kq.ref_node as char))) ref_conn, CONCAT( RTRIM(kq.remote_ip) , ':' , RTRIM(cast(kq.remote_port as char))) remote_ip, kq.host_txcd, ks.comp_flag, kq.trx_act, (case when (kq.trx_amt1 != 0) then kq.trx_amt1 when (kq.trx_amt2 != 0) then kq.trx_amt2 when (kq.trx_amt3 != 0) then kq.trx_amt3 else 0 end) as trx_amt, kq.server_name, kq.ph_type from kq USE INDEX (order_idx) inner join ks on kq.ref_conn = ks.ref_conn and kq.ref_reqs = ks.ref_reqs and kq.ref_node = ks.ref_node and kq.server_name = ks.server_name ";

	$stmt .= " where kq.entry_type = 'S' and ks.entry_type = 'R' and ks.comp_flag in ('C', 'A', 'R', 'T', 'E') and kq.ref_node = 0 and ks.ph_type = 'COMPOSITE' ";

	if (array_key_exists("ref_conn", $contentAry)){
		$ref = '';
		$ref_reqs = '';
		$ref_node = '';
		if (strpos($contentAry['ref_conn'], ':') !== false){
			$tempArry = array();
			$tempArry = explode(':',$contentAry['ref_conn']);
			$ref = $tempArry[0];
			$ref_reqs = $tempArry[1];
			$stmt .= " and kq.ref_conn = '$ref' and kq.ref_reqs = $ref_reqs ";
			if (sizeof($tempArry) > 2)
			{
				$ref_node = $tempArry[2];
				$stmt .= " and kq.ref_node = $ref_node ";
			}
		}
		else{
			$ref = $contentAry['ref_conn'];
			$stmt .= " and kq.ref_conn = '$ref' ";
		}
	}
	else {
		$date; $startdate; $enddate; $addenddate;
		//Check Date and form date in sql
		$date = date('Ymd');
		switch ($contentAry['srchdate']){
			case 'T' :
				$startdate = $date;
				$enddate = date('Ymd', strtotime('+1 days'));
				$stmt .= " and kq.date_logd = $startdate and ks.date_logd between $startdate and $enddate ";
				break;
			case 'R' :
				if (array_key_exists("s_date_logd", $contentAry) && array_key_exists("e_date_logd", $contentAry)){
					$startdate = str_replace('-', '', $contentAry['s_date_logd']);
					$enddate = str_replace('-', '', $contentAry['e_date_logd']);
					$addenddate = date('Ymd', strtotime( $contentAry['e_date_logd'] . '+1 days'));
				}
				elseif (array_key_exists("s_date_logd", $contentAry) && !array_key_exists("e_date_logd", $contentAry)){
					$startdate = str_replace('-', '', $contentAry['s_date_logd']);
					$enddate = $date;
					$addenddate = date('Ymd', strtotime($date . '+1 days'));
				}
				elseif (!array_key_exists("s_date_logd", $contentAry) && array_key_exists("e_date_logd", $contentAry)){
					$startdate = date('Ymd', strtotime('-1 year'));
					$enddate = str_replace('-', '', $contentAry['e_date_logd']);
					$addenddate = date('Ymd', strtotime( $contentAry['e_date_logd'] . '+1 days'));
				}
				else{
					$startdate = $date;
					$enddate = $date;
					$addenddate = date('Ymd', strtotime('+1 days'));
				}
				$stmt .= " and kq.date_logd between $startdate and $enddate and ks.date_logd between $startdate and $addenddate ";
				break;
			case '7' :
				$startdate = date('Ymd', strtotime('-7 days'));
				$enddate = $date;
				$addenddate = date('Ymd', strtotime('+1 days'));
				$stmt .= " and kq.date_logd between $startdate and $enddate and ks.date_logd between $startdate and $addenddate ";
				break;
			case '30' :
				$startdate = date('Ymd', strtotime('-30 days'));
				$enddate = $date;
				$addenddate = date('Ymd', strtotime('+1 days'));
				$stmt .= " and kq.date_logd between $startdate and $enddate and ks.date_logd between $startdate and $addenddate ";
				break;
			default :
				$date = date('Ymd');
				$startdate = $date;
				$enddate = date('Ymd', strtotime('+1 days'));
				$stmt .= " and kq.date_logd = $startdate and ks.date_logd between $startdate and $enddate ";
				break;
			}

			if (array_key_exists("s_time_logd", $contentAry) && array_key_exists("e_time_logd", $contentAry)){
				$starttime = $contentAry["s_time_logd"];
				$endtime = $contentAry["e_time_logd"];
				$stmt .= " and kq.time_logd between $starttime and $endtime ";
			}

			if (array_key_exists("remote_ip", $contentAry)){
				$ip = '';
				$port = '';
				if (strpos($contentAry['remote_ip'], ':') !== false){
					$tempArry = explode(':',$contentAry['remote_ip']);
					$ip = $tempArry[0];
					$port = $tempArry[1];
					$stmt .= " and kq.remote_ip = '$ip' and kq.remote_port = $port ";
				}
				else{
					$ip = $contentAry['remote_ip'];
					$stmt .= " and kq.remote_ip = '$ip' ";
				}
			}

			if (array_key_exists("ph_webservice", $contentAry)){
				$stmt .= " and kq.ph_webservice  like '%" . $contentAry['ph_webservice'] . "%' ";
			}

			if (array_key_exists("trx_ref", $contentAry)){
				$tempref = $contentAry["trx_ref"];
				$stmt .= " and (ks.trx_ref1 = '$tempref' or ks.trx_ref2 = '$tempref' or ks.trx_ref3 = '$tempref' ) ";
			}

			if (array_key_exists("host_sid", $contentAry)){
				$host_sid = $contentAry["host_sid"];
				$stmt .= " and kq.host_sid = '$host_sid' ";
			}

			if (array_key_exists("host_ref", $contentAry)){
				$host_ref = $contentAry["host_ref"];
				$stmt .= " and ks.host_ref = '$host_ref' ";
			}

			if (array_key_exists("host_txcd", $contentAry)){
				$host_txcd = $contentAry["host_txcd"];
				$stmt .= " and kq.host_txcd = '$host_txcd' ";
			}

			if (array_key_exists("trx_cif", $contentAry)){
				$trx_cif = $contentAry["trx_cif"];
				$stmt .= " and kq.trx_cif = $trx_cif ";
			}

			if (array_key_exists("trx_act", $contentAry)){
				$trx_act = $contentAry["trx_act"];
				$stmt .= " and kq.trx_act = $trx_act ";
			}

			if (array_key_exists("comp_flag", $contentAry)){
				$comp_flag  = $contentAry["comp_flag"];
				$stmt .= " and ks.comp_flag = '$comp_flag' ";
			}

			if (array_key_exists("s_trx_amt", $contentAry) && array_key_exists("e_trx_amt", $contentAry)){
				$startamt = $contentAry["s_trx_amt"];
				$endamt = $contentAry["e_trx_amt"];
				$stmt .= " and (kq.trx_amt1 between $startamt and $endamt or kq.trx_amt2 between $startamt and $endamt or kq.trx_amt3 between $startamt and $endamt ) ";
			}

			$stmt .= ' order by kq.date_logd desc, kq.time_logd desc, kq.tims_logd desc ';
	}
	return $stmt;
}

function formatResult($result){
	$temp = $result->Data;
	foreach($temp as $key)
	{
		$temp = $key->comp_flag;
		$temptyp = $key->ph_type;

		switch ($temp) {
			case 'A' :
				$key->comp_flag = 'Accepted';
				break;
			case 'R' :
				$key->comp_flag = 'Rejected';
				break;
			case 'C' :
				$key->comp_flag = 'Completed';
				break;
			case 'T' :
				$key->comp_flag = 'Timeout';
				break;
			case 'E' :
				$key->comp_flag = 'Error';
				break;
			default :
				break;
		}

		if (($key->ph_webservice_type) === 'R'):
			$key->ph_webservice_type = 'REST';
		elseif (($key->ph_webservice_type) === 'S'):
			$key->ph_webservice_type = 'SOAP';
		endif;
	}
	return $result;
}
?>
