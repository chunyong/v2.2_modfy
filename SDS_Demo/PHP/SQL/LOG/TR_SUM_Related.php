<?php

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';

$ref_conn = 'X-Original-Reference: ' . $_POST['ref_conn'];
$ref_reqs = 'X-Original-Count: ' . $_POST['ref_reqs'];
$ref_node = 'X-Original-Node: ' . $_POST['ref_node'];

$date = $_POST['date_logd'];
$nextdate = date('Ymd', strtotime("$date + 1days"));

$stmt = "Select kq.date_logd, kq.time_logd, kq.ref_node, kq.host_sid, kq.host_did, kq.ph_webservice, kq.ph_webservice_type, ks.host_sts, kq.trx_cif, ks.host_ref, ( RTRIM(cast(kq.ref_conn as char)) + ':' + RTRIM(cast(kq.ref_reqs as char)) + ':' + RTRIM(cast(kq.ref_node as char))) ref_conn, ( RTRIM(kq.remote_ip) + ':' + RTRIM(cast(kq.remote_port as char))) remote_ip, kq.host_txcd, ks.comp_flag, kq.trx_act, (case when (kq.trx_amt1 != 0) then kq.trx_amt1 when (kq.trx_amt2 != 0) then kq.trx_amt2 when (kq.trx_amt3 != 0) then kq.trx_amt3 else 0 end) as trx_amt, kq.server_name, kq.ph_type, rq.dta_hdr from kq inner join ks on kq.ref_conn = ks.ref_conn and kq.ref_reqs = ks.ref_reqs and kq.ref_node = ks.ref_node and kq.server_name = ks.server_name inner join rq on kq.date_logd = rq.date_reqs and kq.ref_conn = rq.ref_conn and kq.ref_reqs = rq.ref_reqs and kq.ref_node = rq.ref_node and kq.server_name = rq.server_name Where  kq.date_logd =$date and ks.date_logd between $date and $nextdate";

$stmt = $stmt . ' order by kq.date_logd desc, kq.time_logd desc, kq.tims_logd desc ';

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';

include '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);
$i = 0;
$tempary = Array();
foreach($result as $key)
{
	$temp = $key->comp_flag;
	$tempdata = $key->dta_hdr;

	if ((strpos($tempdata,$ref_conn) !== false) && (strpos($tempdata,$ref_reqs) !== false) && (strpos($tempdata,$ref_node) !== false)){
		switch($temp){
			case 'A' :
				$key->comp_flag = 'Accepted';
				break;
			case 'R' :
				$key->comp_flag = 'Rejected';
				break;
			case 'C' :
				$key->comp_flag = 'Completed';
				break;
			case 'T' :
				$key->comp_flag = 'Timeout';
				break;
			case 'E' :
				$key->comp_flag = 'Error';
				break;
			default :
				break;
		}
		array_push($tempary,$key);
	}
	++$i;
}

$result = $tempary;
echo json_encode($result);

?>
