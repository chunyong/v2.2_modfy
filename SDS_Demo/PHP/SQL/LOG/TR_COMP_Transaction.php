<?php

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';

$nextday = date('Ymd', strtotime($_POST['date_logd'] . " +1 days"));

$refconn = $_POST['ref_conn'];
$refreqs = $_POST['ref_reqs'];
$servername = $_POST['server_name'];
$date		 = $_POST['date_logd'];

$stmt = "Select kq.ph_webservice, kq.ph_webservice_type, kq.host_sid, kq.host_did, ks.host_sts, ks.comp_flag, kq.host_txcd,  kq.trx_act , kq.trx_cif, kq.ref_node from kq inner join ks on kq.ref_conn = ks.ref_conn and kq.ref_reqs = ks.ref_reqs and kq.ref_node = ks.ref_node and kq.server_name = ks.server_name ";
$stmt .= " Where kq.ref_conn = '$refconn' and kq.ref_reqs = '$refreqs' and kq.server_name = '$servername' and kq.date_logd = $date and kq.ref_node != 0 and ks.date_logd between $date and $nextday ";

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';

include '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);

$result = formatResult($result);

echo json_encode($result);

function formatResult($result){
	$temp = $result->Data;
	foreach($temp as $key){
		if (($key->ph_webservice_type) === 'R'):
			$key->ph_webservice_type = 'REST';
		elseif (($key->ph_webservice_type) === 'S'):
			$key->ph_webservice_type = 'SOAP';
		endif;

		switch($key->comp_flag){
			case 'A' :
				$key->comp_flag = 'Accepted';
				break;
			case 'R' :
				$key->comp_flag = 'Rejected';
				break;
			case 'C' :
				$key->comp_flag = 'Completed';
				break;
			case 'T' :
				$key->comp_flag = 'Timeout';
				break;
			default ;
				break;
		}
	}
	return $result;
}

?>
