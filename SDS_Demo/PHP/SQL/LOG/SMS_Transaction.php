<?php

//Initial Value
$stmt = null;
$result = array();
$searchContent = $_POST;
$contentAry = Array();

foreach ($searchContent as $key=>$value){
	if ($value != ''){
		$contentAry[$key] = $value;
	}
}

$stmt = formatSql($contentAry);

$_POST['ACT'] = 'INQ';
$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';

include '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);

$result = formatResult($result);

echo json_encode($result);

function formatSql($contentAry){
	$stmt = "Select date_logd, time_logd, Concat( RTRIM(cast(ref_conn as char)) , ':' , RTRIM(cast(ref_reqs as char)) , ':' , RTRIM(cast(ref_node as char))) ref_conn, Concat( RTRIM(cast(ori_ref_conn as char)) , ':' , RTRIM(cast(ori_ref_reqs as char)) , ':' , RTRIM(cast(ori_ref_node as char))) ori_ref_conn, host_txcd, resp_desc, comp_flag, server_name, sms_provider, phone_num, raw_data from sr where date_logd > 0 ";

	if (array_key_exists("ref_conn", $contentAry)){
		$ref = '';
		$ref_reqs = '';
		$ref_node = '';
		if (strpos($contentAry['ref_conn'], ':') !== false){
			$tempArry = array();
			$tempArry = explode(':',$contentAry['ref_conn']);
			$ref = $tempArry[0];
			$ref_reqs = $tempArry[1];
			$stmt .= " and ref_conn = '$ref' and ref_reqs = $ref_reqs ";
			if (sizeof($tempArry) > 2)
			{
				$ref_node = $tempArry[2];
				$stmt .= " and ref_node = $ref_node ";
			}
		}
		else{
			$ref = $contentAry['ref_conn'];
			$stmt .= " and ref_conn = '$ref' ";
		}
	}
	else{

		$date; $startdate; $enddate; $addenddate;
		//Check Date and form date in sql
		$date = date('Ymd');
		switch ($contentAry['srchdate']){
			case 'T' :
				$stmt .= " and date_logd = $date ";
				break;
			case 'R' :
				if (array_key_exists("s_date_logd", $contentAry) && array_key_exists("e_date_logd", $contentAry)){
					$startdate = str_replace('-', '', $contentAry['s_date_logd']);
					$enddate = str_replace('-', '', $contentAry['e_date_logd']);
				}
				elseif (array_key_exists("s_date_logd", $contentAry) && !array_key_exists("e_date_logd", $contentAry)){
					$startdate = str_replace('-', '', $contentAry['s_date_logd']);
					$enddate = $date;
				}
				elseif (!array_key_exists("s_date_logd", $contentAry) && array_key_exists("e_date_logd", $contentAry)){
					$startdate = date('Ymd', strtotime('-1 year'));
					$enddate = str_replace('-', '', $contentAry['e_date_logd']);
				}
				else{
					$startdate = $date;
					$enddate = $date;
					$addenddate = date('Ymd', strtotime('+1 days'));
				}
				$stmt .= " and date_logd between $startdate and $enddate ";
				break;
			case '7' :
				$startdate = date('Ymd', strtotime('-7 days'));
				$enddate = $date;
				$stmt .= " and date_logd between $startdate and $enddate ";
				break;
			case '30' :
				$startdate = date('Ymd', strtotime('-30 days'));
				$enddate = $date;
				$stmt .= " and date_logd between $startdate and $enddate ";
				break;
			default :
				$date = date('Ymd');
				$stmt .= " and date_logd = $date ";
				break;
			}

			if (array_key_exists("s_time_logd", $contentAry) && array_key_exists("e_date_logd", $contentAry)){
				$starttime = $contentAry["s_time_logd"];
				$endtime = $contentAry["e_date_logd"];
				$stmt .= " and time_logd between $starttime and $endtime ";
			}

			if (array_key_exists("phone_num", $contentAry)){
				$phone_num = $contentAry["phone_num"];
				$stmt .= " and phone_num = '$phone_num' ";
			}

			if (array_key_exists("comp_flag", $contentAry)){
				$comp_flag = $contentAry["comp_flag"];
				$stmt .= " and comp_flag = '$comp_flag' ";
			}

			if (array_key_exists("host_txcd", $contentAry)){
				$host_txcd = $contentAry["host_txcd"];
				$stmt .= " and host_txcd = '$host_txcd' ";
			}
			$stmt .= ' order by date_logd desc, time_logd desc, tims_logd desc ';
	}
	return $stmt;
}

function formatResult($result){
	$temp = $result->Data;
	foreach($temp as $key){
		$key->ori_ref_conn = ( strpos($key->ori_ref_conn, 'X') !== false ? '-' : $key->ori_ref_conn );
		switch ($key->comp_flag){
			case 'A' :
				$key->comp_flag = 'Accepted';
				break;
			case 'R' :
				$key->comp_flag = 'Rejected';
				break;
			case 'C' :
				$key->comp_flag = 'Completed';
				break;
			case 'T' :
				$key->comp_flag = 'Timeout';
				break;
			case 'E' :
				$key->comp_flag = 'Error';
				break;
			default :
				break;
		}
	}
	return $result;
}

?>
