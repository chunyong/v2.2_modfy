<?php

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';

$stmt = "Select date_logd, time_logd, tims_logd, server_name, log_evt, log_dta from ev ";

if (isset($_POST['SrchDate']) && $_POST['SrchDate'] != '' ):
	$stmt = $stmt . "where date_logd = " . str_replace('-','',$_POST['SrchDate']);
else:
	$stmt = $stmt . "where date_logd = " . date('Ymd');
endif;

if (isset($_POST['STime']) && isset($_POST['ETime']) && $_POST['STime'] != '' && $_POST['ETime'] != ''):
	$stmt = $stmt . ' and time_logd between ' . $_POST['STime'] . ' and ' . $_POST['ETime'];
elseif (isset($_POST['STime']) && isset($_POST['ETime']) && $_POST['STime'] != '' && $_POST['ETime'] == ''):
	$stmt = $stmt . ' and time_logd >= ' . $_POST['STime'];
elseif (isset($_POST['STime']) && isset($_POST['ETime']) && $_POST['STime'] == '' && $_POST['ETime'] != ''):
	$stmt = $stmt . ' and time_logd <= ' . $_POST['ETime'];
endif;

//filter by server name
$stmt = ( isset($_POST['SName']) && $_POST['SName'] != '' ? $stmt . " and server_name = '" . trim($_POST['SName']) . "' " : $stmt );
//filter by event type
$stmt = ( isset($_POST['EType']) && $_POST['EType'] != '' && $_POST['EType'] != 'null' ? $stmt . " and " . "log_evt='". $_POST['EType'] . "' " : $stmt );

$stmt = $stmt . " limit " . ($_POST['start'] + 1)  . " , " . $_POST['length'] ;

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';
include '../../function/SQLCaller.php';

$result = $_POST['result'];
$temp["recordsTotal"] = $_POST['length'] + $_POST['start'];
if (isset($_POST['TCount']) && $_POST['TCount'] != ''):
	$temp["recordsFiltered"] = $_POST['TCount'];
else:
	$temp["recordsFiltered"] = 0;
endif;

$temp["data"] = json_decode($result);
echo html_entity_decode(json_encode($temp));

?>
