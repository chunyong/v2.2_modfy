<?php

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';

$webservice = $_POST['ph_webservice'];
$server = $_POST['server_name'];

$refcon = $_POST['ref_conn'];
$refreq = $_POST['ref_reqs'];
$refnod = $_POST['ref_node'];

$date = $_POST['date_logd'];
$nextdate = date('Ymd',strtotime("$date +1 days"));

$stmt = "Select kq.host_sid, kq.host_did, kq.ph_webservice, kq.ph_webservice_type, ks.host_sts, ks.host_ref, CONCAT( RTRIM(cast(kq.ref_conn as char)) , ':' , RTRIM(cast(kq.ref_reqs as char)) , ':' , RTRIM(cast(kq.ref_node as char))) ref_conn, CONCAT( RTRIM(kq.remote_ip) , ':' , RTRIM(cast(kq.remote_port as char))) remote_ip, kq.host_txcd, ks.comp_flag, kq.server_name, kq.ph_type, kq.ph_mmap, kq.trx_act, kq.trx_cif from kq inner join ks on kq.ref_conn = ks.ref_conn and kq.ref_reqs = ks.ref_reqs and kq.ref_node = ks.ref_node and kq.server_name = ks.server_name ";
$stmt = $stmt . " Where kq.ref_conn = '$refcon' and kq.ref_reqs = $refreq and kq.ref_node = $refnod and kq.date_logd = $date and ks.date_logd between $date and $nextdate and kq.ph_webservice = '$webservice' and kq.server_name = '$server' ";

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'LOG';

include  '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);
$temp = $result->Data;
foreach($temp as $key)
{
	$temp = $key->comp_flag;

	switch ($temp) {
		case 'A' :
			$key->comp_flag = 'Accepted';
			break;
		case 'R' :
			$key->comp_flag = 'Rejected';
			break;
		case 'C' :
			$key->comp_flag = 'Completed';
			break;
		case 'T' :
			$key->comp_flag = 'Timeout';
			break;
		case 'E' :
			$key->comp_flag = 'Error';
			break;
		default :
			break;
	}

	if (($key->ph_webservice_type) === 'R'):
		$key->ph_webservice_type = 'REST';
	elseif (($key->ph_webservice_type) === 'S'):
		$key->ph_webservice_type = 'SOAP';
	endif;
}

echo json_encode($result, JSON_UNESCAPED_SLASHES);

?>
