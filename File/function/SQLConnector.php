<?php
/* V2 version General SQL Connector to initial configuration and load with SQL connector
  [$_POST parameter]
	- ACT 	: Action (Ins = insert, Upd = update, Dlt = delete, Ins = insert)
	- STMT 	: SQL statement (sent from SQL function)
	- result: Result to return to SQL function
 	- DBTYP : Database Type (LOG = DBLog, SYS = system database, CONF = parameter database)*/

error_reporting(E_ERROR | E_PARSE);
$xmlDoc = simplexml_load_file(realpath(__DIR__ . '/..') . '/cfg/Config.xml') or die (" SQL Error : Unable to read the config file. ");

$srvname = null;
$srvtyp = null;
$dbname = null;
$dbip = null;
$dbport = null;
$username = null;
$password = null;
$sqlStmt = null;
$action = null;
$enckey = null;
$enciv = null;
if (!class_exists('Util')){
	require_once(realpath(__DIR__ . '/..') . '/function/Util.php');
}
$util = new Util();

if (isset($_POST['DBTYP'])):
	if ($_POST['DBTYP'] === 'LOG'):
		$dbname = $xmlDoc->LOGDBNAME;
	elseif ($_POST['DBTYP'] === 'SYS'):
		$dbname = $xmlDoc->SYSDBNAME;
	elseif ($_POST['DBTYP'] === 'CONF'):
		$dbname = $xmlDoc->CONFDBNAME;
	else:
		die($util->onFail("SQL Error : Invalid Database type from php SQL. "));
	endif;
else:
		die($util->onFail("SQL Error : No Defined DB Type. "));
endif;

if(isset($_POST['STMT'])):
	$sqlStmt = $_POST['STMT'];
else:
	die($util->onFail("SQL Error : Did not receive any query from SQL function."));
endif;

if(isset($_POST['ACT'])):
	$action = $_POST['ACT'];
else:
	die($util->onFail("SQL Error : No action receive."));
endif;

$srvname	= $xmlDoc->SERVERNAME;
$srvtyp		= $xmlDoc->DBTYPE;
$username = $xmlDoc->DBUSERNAME;
$password = $xmlDoc->DBPASSWORD;
$dbip 		= $xmlDoc->DBIP;
$dbport 	= $xmlDoc->DBPORT;
$toprec 	= $xmlDoc->TOPREC;
$dbport 	= (Int)$xmlDoc->DBPORT;
$enckey 	= (String)$xmlDoc->ENCKEY;
$enciv 		= (String)$xmlDoc->ENCIV;

//Check sql type and append top record without needed SQL layer to set
if ($_POST['ACT'] == 'INQ' && $_POST['DBTYP'] == 'LOG'){
	$sqlStmt = $util->SQLChecker($srvtyp, $toprec, $sqlStmt);
}

if (strcasecmp($srvtyp,'MYSQL') == 0):
	$dbtyp = 'mysql';
	include __DIR__ . '/SQL/PDO_Connector.php';
elseif (strcasecmp($srvtyp, 'MSSQL') == 0):
	$dbtype = 'dblib';
	include __DIR__ . '/SQL/PDO_Connector.php';
elseif (strcasecmp($srvtyp, 'ORACLE') == 0):
	$dbtyp = 'oci';
	include __DIR__ . '/SQL/PDO_Connector.php';
else:
	die($util->onFail("Error : Unsupported SQL"));
endif;

?>
