<?php

function HTTP_Trigger($url, $header, $body, $util){
	$field = $body;
	
	//Initial Curl option
	$con = curl_init();

	curl_setopt($con, CURLOPT_URL, $url);
	curl_setopt($con, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($con, CURLOPT_HTTP_VERSION,  CURL_HTTP_VERSION_1_1);
	curl_setopt($con, CURLOPT_HTTPHEADER, $header);
	curl_setopt($con, CURLOPT_ENCODING, 'gzip');
	curl_setopt($con, CURLOPT_POST, 1);
	curl_setopt($con, CURLOPT_POSTFIELDS, $field);
	curl_setopt($con, CURLOPT_RETURNTRANSFER, true);

	$respdata = curl_exec($con);

	if (curl_errno($con)):
		$err = curl_strerror(curl_errno($con));
		curl_close($con);
		$resp = $util->onFail($err);
	else:
		curl_close($con);
		$resp = $util->onSuccess($respdata);
	endif;
		return $resp;
}

?>
