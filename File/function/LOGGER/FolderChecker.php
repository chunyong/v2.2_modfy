<?php

$logfolder = "../log/";
if (!file_exists($logfolder)):
  mkdir("../log/");
endif;

//check first log
if (!file_exists($logfolder . "debuglog0.txt")):
  $f = fopen($logfolder. "debuglog0.txt", "w");
  fclose($f);
endif;

$filelist = glob($logfolder."*.txt", GLOB_BRACE);
if (filesize($logfolder. "debuglog0.txt") > 5000000):
  if (file_exists($logfolder."debuglog9.txt")):
    unlink($logfolder."debuglog9.txt");
  endif;
endif;


foreach (array_reverse($filelist) as $key => $value) {
  $temp = explode("debuglog",$value);
  $temp1 = explode(".", $temp[1]);
  $inc = (int)$temp1[0] + 1;
  rename($value, $logfolder."debuglog$inc.txt");
  echo $value . "  " . $logfolder."debuglog$inc.txt";
}
$f = fopen($logfolder. "debuglog0.txt", "w");
fflush($f);
fclose($f);
?>
