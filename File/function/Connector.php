<?php

//$_POST parameter
//	- prefer: prefer response type
//	- method: HTTP method
//	- body	: body message
//	- path	: url path

//open config file
$xmlDoc = simplexml_load_file(realpath(__DIR__ . '/..') . '/cfg/Config.xml') or die (" HTTP Error : Unable to read the config file. ");

$body = '';

if (!class_exists('Util')){
	require_once(realpath(__DIR__ . '/..') . '/function/Util.php');
	$util = new Util();
}


if (isset($_POST['url']) && $_POST['url'] != ''){
	$url = $_POST['url'];
}
else{
	//initial url
	//Get config value
	$ip = $xmlDoc->HTTPIP;
	$port = $xmlDoc->HTTPPORT;

	//Validate path
	$path = (isset($_POST['path']) && $_POST['path'] != '') ? $_POST['path'] : die(" Error : No path allocate. ");

	//form url
	$url = 'http://' . $ip . ':' . $port . $path;

	if (isset($_POST['prefer']) && $_POST['prefer'] != ''){
		$url .= '?__prefer=' . $_POST['prefer'];
	}
}

//Decode body message with htmlentities
$body = (isset($_POST['data'])) ? html_entity_decode($_POST['data']) : '' ;

$method = (isset($_POST['method'])) ? $_POST['method'] : die(' Error : No method defined!!!');

//Initial Client IP
$client = $_SERVER['REMOTE_ADDR'];
if ($client === '::1' && $_SERVER['HTTP_HOST'] === 'localhost'){
	$client = 'localhost';
}
$timestamp = date('YmdHis');

//Initial HTTP Header
$httpHeader = array(
				'User-Agent: ' . $_SERVER['HTTP_USER_AGENT'],
				'Connection: ' . $_SERVER['HTTP_CONNECTION'],
				'Accept: ' . $_SERVER['HTTP_ACCEPT'],
				'Accept-Encoding: ' . $_SERVER['HTTP_ACCEPT_ENCODING'],
				'Accept-Language: ' . $_SERVER['HTTP_ACCEPT_LANGUAGE'],
				'x-Server-TimeStamp: ' . $timestamp,
				'x-Client-IP-Address: ' . $client
);

if (strcasecmp($_POST['method'], 'GET') == 0):
	include('HTTP/HTTP_GET.php');
elseif (strcasecmp($_POST['method'], 'POST') == 0):
	include('HTTP/HTTP_POST.php');
else:
	die(" Error : Unsupported method. ");
endif;

$result = HTTP_Trigger($url, $httpHeader, $body, $util);

echo $result;

?>
