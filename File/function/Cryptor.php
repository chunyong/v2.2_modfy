<?php

function AES128_ENC($key, $text, $mode, $iv)
{
	$result = '';
	$xmlDoc = simplexml_load_file(realpath(__DIR__ . '/..') . '/cfg/Config.xml') or die (" Unable to read the config file. ");
	if ($key == ''):
		$key = (String)$xmlDoc->ENCKEY;
	endif;
	if ($iv == ''):
		$iv = (String)$xmlDoc->ENCIV;
	endif;
	//$result = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, $mode, hex2bin($iv));
	$result = openssl_encrypt($text, $mode, $key, OPENSSL_RAW_DATA, hex2bin($iv));
	return $result;
}

function AES128_DEC($key, $text, $mode, $iv)
{
	$result = '';
	$xmlDoc = simplexml_load_file(realpath(__DIR__ . '/..') . '/cfg/Config.xml') or die ("Unable to read the config file. ");
	if ($key == ''):
		$key = (String)$xmlDoc->ENCKEY;
	endif;
	if ($iv == ''):
		$iv = (String)$xmlDoc->ENCIV;
	endif;
	//$result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $text, $mode, hex2bin($iv));
	$result = openssl_decrypt($text, $mode, $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, hex2bin($iv));
	$result = preg_replace('/[^\x20-\x7f]/', '', $result);
	return $result;
}

?>
