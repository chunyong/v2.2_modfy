<?php

/*	PDO ver. for Any SQL
	$GLOBAL parameter already predefined in SQL Connector ~~~~~~~~~~~~
		- srvname 	: server name using in MSSQL or Service name in Oracle
		- username 	: Database Username
		- password	: Database Password
		- dbtyp		: Database Connection String
		- dbname	: Database type
		- dbip		: Database IP
		- dbport	: Database Port
		- enckey	: Encryption Key
		- enciv		: Encryption IV
		- sqlStmt	: Sql Statement
		- action	: sql Action ( Inq/ Ins / Upd / Dlt) */

error_reporting(E_ERROR | E_PARSE);

$dsn = "$dbtyp:dbname=$dbname;host=$dbip;port=$dbport";

$rspArry = Array();
$data = Array();
$_POST['result'] = '';

try{
	$con = new PDO($dsn, $username, $password, array(PDO::ATTR_TIMEOUT => "25"));
	// SQL Inquiry Function
	if (strcasecmp('Inq', $action) == 0):
		$result = $con->prepare($sqlStmt);
		$result->execute();
		$sqlrsp = $result->errorInfo();
		$sqlrspcode = $sqlrsp[1];
		$sqlrspdesc = end($sqlrsp);
		if ($sqlrspcode == '' && $sqlrspdesc == ''){
			$data = $result->fetchAll(PDO::FETCH_ASSOC);
			$data = $util->DataChecker($data);
			$result = $util->onSuccess($data);
		}
		else{
			$result = $util->onFail($sqlrspdesc);
			exit($result);
		}
	// SQL Maintainence Function (UPD , INS , DLT)
	elseif (strcasecmp('Ins', $action) == 0 || strcasecmp('Upd', $action) == 0 || strcasecmp('Dlt', $action) ==0):
		$con->beginTransaction();
		$result = $con->exec($sqlStmt);
		$mntrsp = '';
		switch ($action){
			case strcasecmp('Ins', $action) :
				$mntrsp = 'Data Inserted.';
				break;
			case strcasecmp('Upd', $action) :
				$mntrsp = 'Data Updated.';
				break;
			case strcasecmp('Dlt', $action) :
				$mntrsp = 'Data Deleted.';
				break;
			default :
				$mntrsp = 'Nothing Happen to the Data.';
				break;
		}
		$sqlrsp = $con->errorInfo();
		$sqlrspcode = $sqlrsp[1];
		$sqlrspdesc = end($sqlrsp);
		if ($sqlrspcode == '' && $sqlrspdesc == ''){
			$con->commit();
			$result = $util->onSuccess($mntrsp);
		}
		else{
			$result = $util->onFail($sqlrspdesc);
			exit($result);
		}
	//Not support action
	else :
		$result = $util->onFail("Unsupported Action.");
	endif;
} catch (PDOException $ex){
	$msg = $ex->getMessage();
	$result = $util->onFail($msg);
	exit($result);
}
$_POST['result'] = $result;
$con = null;

?>
