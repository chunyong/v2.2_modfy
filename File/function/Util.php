<?php

class Util {

	public function __construct(){
		if (!function_exists('AES128_ENC') ) {
    	require(__DIR__ . '/Cryptor.php');
		}
	}

	//parse Success result
	public function onSuccess($data){
		$result = array();
		$result['Status'] = 'AA';
		$result['Data'] = $data;
		$jsonresult = json_encode($result,JSON_UNESCAPED_SLASHES);
		return $jsonresult;
	}

	//parse fail result
	function onFail($data){
		$result = array();
		$result['Status'] = 'AB';
		$result['Data'] = $data;
		$jsonresult = json_encode($result,JSON_UNESCAPED_SLASHES);
		return $jsonresult;
	}

	//format date
	function formatDate($date){
		return date('Y-m-d', strtotime($date));
	}

	//format time
	function formatTime($time){
		return gmdate("H:i:s", $time);
	}

	//format amount
	function formatAmount($amount){
		return number_format($amount,2);
	}

	//call encrypt function
	function encData($key,$val,$mode,$iv){
		$data = AES128_ENC($key,$val,$mode,$iv);
		return $data;
	}

	//call decrypt function
	function decData($key,$val,$mode,$iv){
		$data = AES128_DEC($key,$val,$mode,$iv);
		return $data;
	}

	//check raw_data field for decrypt
	function DataChecker($data){
		if (!function_exists('encode_items')) {
			function encode_items(&$item, $key)
			{
				switch($key){
					case 'kq_raw_data':
					case 'ks_raw_data':
						if (bin2hex($item) == '00' || $item == null):
							$item = null;
						else:
							$item = trim(AES128_DEC($GLOBALS['enckey'],$item,'AES-128-CBC',$GLOBALS['enciv']));
							$item = htmlentities(utf8_encode($item));
						endif;
						break;
					case 'PASSWORD':
						if (bin2hex(base64_decode($item)) == '00'):
							$item = null;
						else:
							$item = trim(AES128_DEC($GLOBALS['enckey'],base64_decode($item),'AES-128-CBC',$GLOBALS['enciv']));
						endif;
						break;
					case 'codepage':
						$GLOBALS['codepage'] = trim($item);
						break;
					case 'raw_data':
						if (bin2hex($item) == '00' || $item == null):
							$item = null;
						elseif ($GLOBALS['codepage'] == 'UTF-8' || $GLOBALS['codepage'] == 'ASCII'):
							$item = trim(AES128_DEC($GLOBALS['enckey'],$item,'AES-128-CBC',$GLOBALS['enciv']));
							$item = htmlentities(utf8_encode($item));
						elseif (!isset($GLOBALS['codepage'])):
							$item = trim(AES128_DEC($GLOBALS['enckey'],$item,'AES-128-CBC',$GLOBALS['enciv']));
							$item = html_entity_decode(utf8_encode($item));
						else:
							$item = AES128_DEC($GLOBALS['enckey'],$item,'AES-128-CBC',$GLOBALS['enciv']);
							$item = "0x" . strtoupper(bin2hex($item));
						endif;
						break;
					default:
						$item = utf8_encode($item);
						break;
				}
			}
		}
		for ($i = 0 ; $i < sizeof($data); $i++){
			array_walk_recursive($data[$i], 'encode_items');
		}
		return $data;
	}

	//Check and add search top query to Inquiry SQL
	Public Function SQLChecker($srvtyp, $toprec, $sqlStmt){
		$act = $_POST['ACT'];
		$dtyp = $_POST['DBTYP'];
		if (strcasecmp($srvtyp,'MYSQL') == 0){
			if (stripos($sqlStmt, "LIMIT") === false){
				$sqlStmt = (substr_count(strtoupper($sqlStmt), "LIMIT") == 0) ? 	$sqlStmt . " LIMIT $toprec " : $sqlStmt ;
			}
		}
		elseif (strcasecmp($srvtyp, 'MSSQL') == 0){
			if (stripos($sqlStmt, "TOP") === false){
				$sqlStmt = (substr_count(strtoupper($sqlStmt), "TOP") == 0) ? preg_replace('/Select/', "Select TOP $toprec ", $sqlStmt, 1) : $sqlStmt ;
			}
		}
		elseif (strcasecmp($srvtyp, 'ORACLE') == 0){
			$cntwhere = substr_count(strtoupper($sqlStmt), "WHERE");
			$cntorder = substr_count(strtoupper($sqlStmt), "ORDER BY");
			$cntgroup = substr_count(strtoupper($sqlStmt), "GROUP BY");
			if ($cntwhere > 1):
				$sqlStmt = preg_replace('~where(?!.*where)~i', "Where ROWNUM <= $toprec ", $sqlStmt);
			elseif (($cntwhere == 0 && $cntorder >= 1) || ($cntwhere == 0 && $cntgroup >= 1)):
				$sqlStmt = preg_replace('~order by (?!.*order by)~i',"Where ROWNUM <= $toprec Order by " , $sqlStmt);
			elseif ($cntwhere == 0 && $cntgroup >= 1 && $cntorder == 0):
				$sqlStmt = preg_replace('~group by (?!.*group by)~i',"Where ROWNUM <= $toprec Group by " , $sqlStmt);
			else:
				$sqlStmt = $sqlStmt . " Where ROWNUM <= $toprec ";
			endif;
		}
		return $sqlStmt;
	}
}

?>
