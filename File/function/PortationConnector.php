<?php

//$_POST parameter
//	- prefer: prefer response type
//	- method: HTTP method
//	- body	: body message
//	- path	: url path

//open config file
$xmlDoc = simplexml_load_file('../../../../File/cfg/Config.xml') or die(" Error : Config file not found.");

//initial url
//Get config value
$ip = $xmlDoc->PRTIP;
$port = $xmlDoc->PRTPORT;
$DBTYP = $xmlDoc->PRTDBTYP;
$DBSCH = $xmlDoc->PRTDBSCH;
$tablelist = $xmlDoc->PRTTABLE;
$SDSIP = $xmlDoc->EXPIP;
$SDSPORT = $xmlDoc->EXPPORT;

$DBIP = $xmlDoc->DBIP;
$DBPORT = $xmlDoc->DBPORT;
$DBNAME = $xmlDoc->CONFDBNAME;
$username = $xmlDoc->DBUSERNAME;
$password = $xmlDoc->DBPASSWORD;
$constring = $xmlDoc->CONSTRING;
$body = '';

$action = (!isset($_POST['action'])) ? die("Error : No action provide!!! Export or Import") : $_POST['action'];

$file = (!isset($_POST['file'])) ? die("Error : No file provide!!! ") : $_POST['file'];

//form url
$url = 'http://' . $ip . ':' . $port;

$method = 'POST';

//Initial Client IP
$client = $_SERVER['REMOTE_ADDR'];
$client = ($_SERVER['REMOTE_ADDR'] === '::1' && $_SERVER['HTTP_HOST'] === 'localhost') ? 'localhost' : $_SERVER['REMOTE_ADDR'];

$timestamp = date('YmdHis');

$uri = $constring . '//' . $DBIP . ':' . $DBPORT . ';' . 'databaseName=' . $DBNAME;

$body = 'ConnectionString=' . $uri . hex2bin('0a') .
		'Username=' . $username . hex2bin('0a') .
		'Password=' . $password . hex2bin('0a') .
		'Table=' . $tablelist . hex2bin('0a') .
		'DbName=' . $DBNAME . hex2bin('0a') .
		'Type=' . $DBTYP . hex2bin('0a') .
		'Schema=' . $DBSCH . hex2bin('0a') .
		'SDSIP='.  $SDSIP . hex2bin('0a') .
		'SDSPORT=' . $SDSPORT . hex2bin('0a');

$body = bin2hex($body);

//Initial HTTP Header
$httpHeader = array(
				'User-Agent: ' . $_SERVER['HTTP_USER_AGENT'],
				'Connection: ' . $_SERVER['HTTP_CONNECTION'],
				'Accept: ' . $_SERVER['HTTP_ACCEPT'],
				'Accept-Encoding: ' . $_SERVER['HTTP_ACCEPT_ENCODING'],
				'Accept-Language: ' . $_SERVER['HTTP_ACCEPT_LANGUAGE'],
				'Content-Type: text/plain',
				'Content-Length: ' . strlen($body),
				'x-Server-TimeStamp: ' . $timestamp,
				'x-Client-IP-Address: ' . $client,
				'x-action: ' . $action,
				'x-file: '	. $file,
				'x-encoding: utf-8',
				'x-hash: ' . md5($body)
);

include('HTTP/HTTP_POST.php');

$result = HTTP_Trigger($url, $httpHeader, $body);

echo hex2bin($result);

?>
