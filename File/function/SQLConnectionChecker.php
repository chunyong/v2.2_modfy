<?php
/* V2 version General SQL Connector to initial configuration and load with SQL connector
  [$_POST parameter]
	- ACT 	: Action (Ins = insert, Upd = update, Dlt = delete, Ins = insert)
	- STMT 	: SQL statement (sent from SQL function)
	- result: Result to return to SQL function
 	- DBTYP : Database Type (LOG = DBLog, SYS = system database, CONF = parameter database)*/

$xmlDoc = simplexml_load_file(realpath(__DIR__ . '/..') . '/cfg/Config.xml') or die (" Error : Unable to read the config file. ");

$srvname = null;
$srvtyp = null;
$dbname = null;
$dbip = null;
$dbport = null;
$username = null;
$password = null;
$sqlStmt = null;
$action = null;
$enckey = null;
$enciv = null;

require_once(realpath(__DIR__ . '/..') . '/function/Util.php');
$util = new Util();

$srvname	= $xmlDoc->SERVERNAME;
$srvtyp		= $xmlDoc->DBTYPE;
$username = $xmlDoc->DBUSERNAME;
$password = $xmlDoc->DBPASSWORD;
$dbip 		= $xmlDoc->DBIP;
$toprec 	= $xmlDoc->TOPREC;
$dbport 	= (Int)$xmlDoc->DBPort;
$dbname = $xmlDoc->SYSDBNAME;
$dbname = $xmlDoc->LOGDBNAME;
$dbname = $xmlDoc->CONFDBNAME;

if (strcasecmp($srvtyp,'MYSQL') == 0):
	$dbtyp = 'mysql';
elseif (strcasecmp($srvtyp, 'MSSQL') == 0):
	$dbtype = 'dblib';
elseif (strcasecmp($srvtyp, 'ORACLE') == 0):
	$dbtyp = 'oci';
else:
	die("Error : Unsupported SQL");
endif;

$dsn = "$dbtyp:dbname=$dbname;host=$dbip;port=$dbport";
try{
	$con = new PDO($dsn, $username, $password);
	echo ("Connection good.");
}catch (PDOException $ex){
	$msg = $ex->getMessage();
	die($msg);
}
$con = null;


?>
