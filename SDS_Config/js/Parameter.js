/*

	SDS API - Comman Functions
	Do not make Changes Without Notice in this API, it may AFFECT System Structure/Flow
	==============================================================================================================
	| Code		| Username		| Date			| Function Name		| Remarks

	===============================================================================================================

*/
	/** PreLoad Language Transalate **/

	/* SDS Console Script Path*/
	var script_path = {
		"User Search" : "PHP/SQL/USER/User_Inquiry.php",
		"User Group List" : "PHP/SQL/USER/Group_Listing.php",
		"Update User"	: "PHP/SQL/USER/Update_User.php",
		"Add User" 		: "PHP/SQL/USER/Add_User.php",
		"Remove User"	: "PHP/SQL/USER/Remove_User.php",
		"User Group Search" : "PHP/SQL/USER/Group_Inquiry.php",
		"Add User Group"	: "PHP/SQL/USER/Add_Group.php",
		"Remove User Group" : "PHP/SQL/USER/Remove_Group.php",
		"Menu Search" : "PHP/SQL/MENU/ParentMenu_Inquiry.php",
		"Menu Detail" : "PHP/SQL/MENU/ParentMenuDetail_Inquiry.php",
		"Remove Menu" : "PHP/SQL/MENU/Remove_Menu.php",
		"Add Menu"		: "PHP/SQL/MENU/Add_ParentMenu.php",
		"Update Menu" : "PHP/SQL/MENU/Update_MENU.php",
		"Menu System List" : "PHP/SQL/MENU/MenuSys_Inquiry.php",
		"Sub Menu Search" : "PHP/SQL/MENU/SubMenu_Inquiry.php",
		"Sub Menu Detail" : "PHP/SQL/MENU/SubMenuDetail_Inquiry.php",
		"Add Sub Menu": "PHP/SQL/MENU/Add_SubMenu.php",
		"Update Sub Menu" : "PHP/SQL/MENU/Update_SubMENU.php",
		"Group Access Search" : "PHP/SQL/ACCESS/Group_Inquiry.php",
		"Menu Access Search" : "PHP/SQL/ACCESS/MenuAcc_Inquiry.php",
		"Remove Menu Access" : "PHP/SQL/ACCESS/Remove_Menu.php",
		"Menu List"				: "PHP/SQL/ACCESS/Menu_Inquiry.php",
		"Add Menu Access" : "PHP/SQL/ACCESS/Add_Access.php",
		"Menu System List": "PHP/SQL/ACCESS/MenuSys_Inquiry.php",
		"Get Config"		 	: "PHP/SQL/UTIL/GetConfig.php",
		"Put Config"			: "PHP/SQL/UTIL/PutConfig.php"
	};
