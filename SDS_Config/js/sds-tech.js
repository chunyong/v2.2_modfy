/*

	SDS API - Comman Functions
	Do not make Changes Without Notice in this API, it may AFFECT System Structure/Flow
	==============================================================================================================
	| Code		| Author			| Date			 | Function Name		| Remarks
	| 00			| ChunYong		| 02/05/2018 | triggerHTTP			| Ajax call HTTP Caller
	===============================================================================================================

*/
	/** PreLoad Language Transalate **/
	$.getScript( "js/parameter.js" );

	$(".side-bar-button").click(function(){
			$("body").toggleClass("mini-navbar");
			$(".side-bar-button").addClass("hidden");
	});

	$("#page-wrapper").mouseover(function(){
			$("body").addClass("mini-navbar");
			$(".side-bar-button").removeClass("hidden");
	});

function backHome(){
		$("#mainContent").load("pages/SDS_HOME/About_Us.html");
}

function bypass(){
		userLogin('ADMIN', '101010', 'Console');
}

function showMore(){
	var chk = $(".show-more").text();
	if (chk.includes("Show More") == true){
		$(".hidden-box").fadeIn();
		$(".hidden-box").removeClass("hidden");
		$(".show-more").html('Show Less Search Options <i class="fa fa-angle-double-up"></i>');
	}
	else{
		$(".hidden-box").fadeOut();
		$(".hidden-box input").val("");
		$(".show-more").html('Show More	 Search Options <i class="fa fa-angle-double-down"></i>');
	}
}

function triggerAjax(option, callback){
	var rspdata;
	var opt = {
		script_id : '',
		method : '',
		data : []
	};
	$.extend(opt, option);
	$.ajax({
			"url"	: script_path[opt.script_id],
			"type"	: opt.method,
			"data"  : opt.data
		})
	.done(function(data){
		toastr.options = {
					closeButton: true,
					progressBar: true,
					preventDuplicates: true,
					showMethod: 'slideDown',
					timeOut: 5000
		};
		try{
			rspdata = JSON.parse(data);
			var status = rspdata.Status;

			if (status == 'AB'){
					toastr['error'](rspdata.Data);
			}
			setTimeout(function(){return callback(rspdata);}, 600);
		}catch(err){
			alert(err);
			alert(data);
		}
	});
}

function getWatchList(data){
	localStorage.clear()
	var size = Object.keys(data).length;
	var key  = Object.keys(data);
	var val  = Object.values(data);
	for (var i = 0; i < size; i++){
		localStorage.setItem(key[i], val[i]);
	}
	var win = window.open('Watchlist_DTL.html', '_blank');
	win.focus();
}

function getWatchListKey(){
	var obj = {};
	for ( var i = 0, len = localStorage.length; i < len; ++i ) {
		obj[localStorage.key( i )] = localStorage.getItem( localStorage.key( i ) );
	}
	return obj;
}

function triggerHTTP(option, callback){
	var rspdata;
	var opt = {
		url : '',
		path : '',
		method : '',
		prefer : '',
		data : []
	};
	$.extend(opt, option);
	$.ajax({
			url	 : "PHP/Function/HTTPCaller.php",
			type : "POST",
			data : {
				"method" : opt.method,
				"url"		 : opt.url,
				"path"	 : opt.path,
				"prefer" : opt.prefer,
				"data"	 : opt.data
			}
		})
	.done(function(data){
		toastr.options = {
					closeButton: true,
					progressBar: true,
					preventDuplicates: true,
					showMethod: 'slideDown',
					timeOut: 5000
		};
		try{
			rspdata = JSON.parse(data);
			var status = rspdata.Status;

			if (status == 'AB'){
					toastr['error'](rspdata.Data);
			}
			setTimeout(function(){return callback(rspdata);}, 600);
		}catch(err){
			alert(err);
			alert(data);
		}
	});
}
