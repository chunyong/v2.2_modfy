<?php

require(realpath(__DIR__ . '/../../../..') . '/File/function/Util.php');
$util = new Util();
if (file_exists(realpath(__DIR__ . '/../../../..') . '/File/cfg/Config.xml')) {
  $xml = simplexml_load_file(realpath(__DIR__ . '/../../../..') . '/File/cfg/Config.xml');
  $json = $util->onSuccess(json_encode($xml));
}
else{
  $json = $util->onFail("Config file not found.");
}

echo $json;

?>
