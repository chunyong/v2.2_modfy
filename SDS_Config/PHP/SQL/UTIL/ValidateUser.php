<?php

//Initial Value
$stmt = null;
$result = array();

$_POST['ACT'] = 'INQ';
$_POST['DBTYP'] = 'SYS';
$username = $_POST['username'];

$stmt = "Select U.LOGIN_ID, U.NAME, U.PASSWORD, G.NAME as UGROUP, G.GROUP_ID from USERS U inner join USER_GROUP UG on U.USER_ID = UG.USER_ID inner join U_GROUP G on UG.GROUP_ID = G.GROUP_ID";
$stmt .= " Where U.LOGIN_ID = '$username' and U.ACTIVE='Y' ";

$_POST['STMT'] = $stmt;
include '../../function/SQLCaller.php';
$result = json_decode($_POST['result']);

$pass = '';
$group = '';
$login = '';
$name = '';
$groupid = '';

session_start();
$usrresult = $result->Data;
if (sizeof($usrresult) > 0)
{
	foreach($usrresult as $key)
	{
		$pass = $key->PASSWORD;
		$group = $key->UGROUP;
		$login = $key->LOGIN_ID;
		$name	= $key->NAME;
		$groupid = $key->GROUP_ID;
	}
	if ($_POST['password'] == trim($pass)):
		if ($group != 'Administrator'):
			$_POST['ACT'] = 'INQ';
			$_POST['DBTYP'] = 'SYS';
			$_POST['result'] = '';
			$stmt = " Select M.MENU_ID from MENU M inner join MENU_GROUP MG on M.MENU_ID = MG.MENU_ID inner join U_GROUP G on MG.GROUP_ID = G.GROUP_ID";
			$stmt .= " Where G.NAME = '" . $group . "' and G.GROUP_ID ='" . $groupid . "' and M.SYS = '" . $_POST['sys'] . "' ";
			$_POST['STMT'] = $stmt;
			include '../../function/SQLCaller.php';
			$mnuresult = json_decode($_POST['result']);
			if (sizeof($mnuresult->Data) > 0)
			{
				$_SESSION['LOGIN_ID'] = $login;
				$_SESSION['GROUP'] = $group;
				$_SESSION['NAME'] = $name;
				$_SESSION['GROUP_ID'] = $groupid;
				$_SESSION['SYS'] = 'SDS ' . $_POST['sys'];
			}
			else
			{
				$mnuresult->Status = 'AB';
				$mnuresult->Data = "Error : User has no authority in this system. Login not allowed.";
				die(json_encode($mnuresult,JSON_UNESCAPED_SLASHES));
			}
		else:
			$_SESSION['LOGIN_ID'] = $login;
			$_SESSION['GROUP'] = $group;
			$_SESSION['NAME'] = $name;
			$_SESSION['GROUP_ID'] = $groupid;
			$_SESSION['SYS'] = 'SDS ' . $_POST['sys'];
		endif;
	else:
		$result->Status = 'AB';
		$result->Data = "Error : Invalid Passowrd. ";
		die(json_encode($result,JSON_UNESCAPED_SLASHES));
	endif;
}
else
{
	include '../../SQL/UTIL/DestroySession.php';
	$result->Status = 'AB';
	$result->Data = "Error : Username not found or User is not active.";
	die(json_encode($result,JSON_UNESCAPED_SLASHES));
}
$result->Status = 'AA';
$result->Data = "User login.";
echo json_encode($result, JSON_UNESCAPED_SLASHES);
?>
