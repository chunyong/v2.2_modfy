<?php

require(realpath(__DIR__ . '/../../../..') . '/File/function/Util.php');
$util = new Util();
$content = $_POST['content'];
if (file_exists(realpath(__DIR__ . '/../../../..') . '/File/cfg/Config.xml')) {
  $xml = simplexml_load_file(realpath(__DIR__ . '/../../../..') . '/File/cfg/Config.xml');
	foreach ($xml->children() as $mkey=>$mval){
		foreach ($content as $key=>$val){
			if ($mkey == $key){
				$xml->$mkey = $val;
				break;
			}
		}
	}
	file_put_contents(realpath(__DIR__ . '/../../../..') . '/File/cfg/Config.xml', $xml->saveXML());
	$result = $util->onSuccess("Config file updated.");
}
else{
	$result = $util->onFail("Config file not found.");
}

echo $result;

?>
