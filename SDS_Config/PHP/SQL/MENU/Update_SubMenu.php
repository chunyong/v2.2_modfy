<?php

//Initial Value
$stmt = null;
$result = array();
require (realpath(__DIR__ . "/../../../..") . "/File/function/Util.php");
$util = new Util();

$_POST['ACT'] = 'INQ';
$stmt = " Select MENU_ID, MENU_TITLE from MENU Where MENU_ID = '" . $_POST['MENU_ID'] . "' ";

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'SYS';
$_POST['SYS'] = 'Demo';

include '../../function/SQLCaller.php';
$result = json_decode($_POST['result']);
if (sizeof($result->Data) > 0)
{
	$_POST['ACT'] = 'UPD';

	if (isset($_POST['PARENT_LEVEL']))
	{
		$level = str_pad($_POST['PARENT_LEVEL'],3,"0",STR_PAD_RIGHT);
		$level .= str_pad($_POST['INDEX_LEVEL'],3,"0",STR_PAD_LEFT);
	}
	else
	{
		$level = $_POST['INDEX_LEVEL'];
	}
	$stmt = " Update MENU Set MENU_TITLE = '" . $_POST['MENU_TITLE'] . "' ,  INDEX_LEVEL = '" . $level . "' , MENU_ICON = '" . $_POST['MENU_ICON'] . "' , HREF = '" . $_POST['HREF'] . "' ";
	$stmt .= " Where MENU_ID = '" . $_POST['MENU_ID'] . "' ";

	$_POST['STMT'] = $stmt;
	$_POST['DBTYP'] = 'SYS';

	include '../../function/SQLCaller.php';

	$result = json_decode($_POST['result']);
	if ($result->Status == 'AA'){
		$result->Data = "Menu Updated";
	}
	$result = json_encode($result, JSON_UNESCAPED_SLASHES);
}
else
{
	$result = $util->onFail("Menu not found!! Deleted ??");
}

echo $result;

?>
