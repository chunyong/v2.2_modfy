<?php

//Initial Value
$stmt = null;
$result = array();
require (realpath(__DIR__ . "/../../../..") . "/File/function/Util.php");
$util = new Util();

$_POST['ACT'] = 'INQ';
$stmt = " Select MENU_ID, MENU_TITLE, INDEX_LEVEL, SYS from MENU Where MENU_ID = '" . $_POST['MENU_ID'] . "' ";

$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'SYS';
$_POST['SYS'] = 'Demo';

include '../../function/SQLCaller.php';
$result = json_decode($_POST['result']);
if (sizeof($result->Data) > 0)
{
	$result = $util->onFail("Menu ID used.");
	die($result);
}

$level = str_pad($_POST['PARENT_LEVEL'],3,"0",STR_PAD_RIGHT);
$level .= str_pad($_POST['INDEX_LEVEL'],3,"0",STR_PAD_LEFT);

if ($_POST['INDEX_LEVEL'] != '')
{
	$stmt = " Select MENU_ID, MENU_TITLE, INDEX_LEVEL, SYS from MENU Where INDEX_LEVEL = '" . $level . "' and  SYS = '" . $_POST['SYS'] . "' ";

	$_POST['STMT'] = $stmt;
	$_POST['DBTYP'] = 'SYS';

	include '../../function/SQLCaller.php';
	$result = json_decode($_POST['result']);
	if (sizeof($result->Data) > 0)
	{
		$result = $util->onFail("Index Level used.");
		die($result);
	}
}

$_POST['ACT'] = 'INS';

$stmt = " Insert into MENU(MENU_ID, PARENT_ID, MENU_TITLE, INDEX_LEVEL, MENU_ICON, HREF, SYS) ";

$stmt .= "Values('" . $_POST['MENU_ID'] . "', '". $_POST['PARENT'] . "', '" . $_POST['MENU_TITLE'] . "', '" . $level . "', '" . $_POST['MENU_ICON'] . "', '" . $_POST['HREF'] . "', '" . $_POST['SYS'] . "' )";


$_POST['STMT'] = $stmt;
$_POST['DBTYP'] = 'SYS';

include '../../function/SQLCaller.php';

$result = json_decode($_POST['result']);

if ($result->Status == 'AA'){
	$result->Data = "Menu Added. ";
}

echo json_encode($result,JSON_UNESCAPED_SLASHES);

?>
